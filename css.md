####Css 规范

* 使用两空格代替tab
* Property `:` 后加空格； Rule `{` 前加空格
* 除rgba颜色外，颜色一律用16进制形式
* 当Property值为0时，不加单位；小于1的小数，小数点前0省略
* id、class命名使用`-` 连接，使用小写，语义化命名，在能理解的情况下尽可能的短，通用，使用英语命名（查字典）
* 尽量使用class
* `,` 后加空格
* 避免使用css hack、css expression
* 避免多余的祖先选择器

######举个栗子：

        // This is just a example!
        .guide-format {
          border: 1px solid #f0f;
          color: #000;
          background: rgba(0, 0, 0, .5);
          padding: 0 10px;
          font-size: .8em;
        }
